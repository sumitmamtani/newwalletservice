package com.bootcamp.wallet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
class WalletController {
    @Autowired
    private WalletService walletService;

    @GetMapping("/wallet")
    Wallet wallet(@RequestParam("username") String username) {
        return walletService.fetchWallet(username);
    }

    @PostMapping(path = "/wallet")
    Wallet wallet(@RequestBody Wallet wallet) {
        return walletService.createWallet(wallet);
    }

    @PutMapping(path = "/wallet")
    Wallet walletPut(@RequestBody Wallet wallet) {
        return walletService.updateBalance(wallet);
    }
    @PutMapping(path = "/debitwallet")
    Wallet walletPutdebit(@RequestBody Wallet wallet) {
        return walletService.debitbalance(wallet);
    }
}
