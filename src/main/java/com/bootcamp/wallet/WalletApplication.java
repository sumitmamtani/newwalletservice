package com.bootcamp.wallet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class WalletApplication {
    @Bean
    List<Wallet> createWallets() {
        return new ArrayList<Wallet>();
    }

    public static void main(String[] args) {
        SpringApplication.run(WalletApplication.class, args);
    }
}

