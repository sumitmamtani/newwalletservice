package com.bootcamp.wallet;

import javax.persistence.*;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.NonNull;

@Entity
@Table(name="Transaction")
class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int userId;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="Id")
    private Wallet wallet;

    @Enumerated(EnumType.STRING)
    @Column(name="type")
    private TransactionType type;

    @Column(name="balance")
    private int balance;

    @Column(name="creationDate")
    @Temporal(TemporalType.DATE)
    private java.util.Date timestamp;


    public Transaction(int usereId, Wallet wallet, String timeStamp, boolean credit, int balance) {
        this.userId = userId;
        this.wallet= wallet;

        this.type = type;
        this.balance = balance;
    }

}
