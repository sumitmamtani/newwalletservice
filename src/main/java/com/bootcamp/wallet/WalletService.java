package com.bootcamp.wallet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
class WalletService {
    @Autowired
    private WalletRepository walletRepository;

    WalletService(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }

    Wallet fetchWallet(String username) {
        List<Wallet> wallets = walletRepository.findByUsername(username);
        return wallets.get(0);
    }

    Wallet createWallet(Wallet wallet) {
        walletRepository.save(wallet);
        return wallet;
    }
    Wallet updateBalance(Wallet wallet ){

        Wallet temp= fetchWallet(wallet.username);
        temp.updatebalance(wallet);
        walletRepository.save(temp);

        return temp;
    }
    Wallet debitbalance(Wallet wallet ){

        Wallet temp= fetchWallet(wallet.username);
        temp.debitbalance(wallet);
        walletRepository.save(temp);

        return temp;
    }
}

