package com.bootcamp.wallet;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

interface TransactionRepository extends CrudRepository<Transaction,Long>{
    List<Transaction> findbyUsername(Wallet wallet);
}