package com.bootcamp.wallet;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
class Wallet {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Size(min=5,max=12)
    private Long Id;
    @JsonProperty
    String username;
    @JsonProperty
    @Range(min=0,max=100000)
    private int balance;

    Wallet() {
    }

    Wallet(String username, int balance) {
        this.username = username;
        this.balance = balance;
    }
    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL,mappedBy = "wallet")
    Set<Transaction> transactionSet=new HashSet<Transaction>();

//    public void updateTransaction(){
//        this.balance+=updated.
//    }

    @Override
    public int hashCode() {
        return Objects.hash(username, balance);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Wallet wallet = (Wallet) o;
        return balance == wallet.balance &&
                Objects.equals(username, wallet.username);
    }

    public void updatebalance(Wallet temp) {
        this.balance=this.balance+temp.balance;
    }
    public void debitbalance(Wallet temp) {
        this.balance=this.balance-temp.balance;
    }


}
