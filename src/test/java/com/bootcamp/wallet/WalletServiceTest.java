package com.bootcamp.wallet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class WalletServiceTest {
    private Wallet wallet;
    @Autowired
    private WalletRepository walletRepository;

    private WalletService walletService;

    @BeforeEach
    void init() {
        walletService = new WalletService(walletRepository);
    }

    @Test
    void shouldReturnRightWalletForUser() throws Exception {
        Wallet wallet = new Wallet("Sumit", 10);
        walletService.createWallet(wallet);
        assertEquals(walletService.fetchWallet("Sumit"), wallet);
    }

    @Test
    void shouldCreateWallet1() throws Exception {
        Wallet wallet = new Wallet("Sumit", 10);
        assertEquals(wallet, walletService.createWallet(wallet));
    }
    @Test
    void shouldCreateWallet2() throws Exception {
        Wallet wallet = new Wallet("Sumit", 10);
        //assertEquals(wallet, walletService.createWallet(wallet));
        assertEquals(wallet,walletService.debitbalance(wallet));
    }
}
