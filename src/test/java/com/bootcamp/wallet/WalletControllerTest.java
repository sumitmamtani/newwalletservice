package com.bootcamp.wallet;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WalletController.class)
class WalletControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WalletService walletService;

    @Test
    void testControllerForGetWallet() throws Exception {
        when(walletService.fetchWallet("Shreya")).thenReturn(new Wallet("Shreya", 10));

        mockMvc.perform(get("/wallet?username=Shreya"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"username\":\"Shreya\",\"balance\":10}"));
    }

    @Test
    void testControllerForPostWallet() throws Exception {
        when(walletService.createWallet(new Wallet("Sumit", 10))).thenReturn(new Wallet("Sumit", 10));

        mockMvc.perform(post("/wallet")
                .content("{\"username\":\"Sumit\",\"balance\":10}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"username\":\"Sumit\",\"balance\":10}"));
    }
    @Test
    void testControllerFordebitPoWallet() throws Exception {
        when(walletService.debitbalance( new Wallet("Sumit", 5))).thenReturn(new Wallet("Sumit", 3));
        mockMvc.perform(put("/debitwallet")
                .content("{\"username\":\"Sumit\",\"balance\":5}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"username\":\"Sumit\",\"balance\":3}"));
    }
}
